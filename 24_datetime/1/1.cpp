#include <iostream>
#include <string>
#include <vector>
#include <ctime>

struct entry
{
    std::string title;
    std::tm timeStart;
    std::tm timeComplete;
    bool isComplete;
};

void CompleteLast(std::vector<entry> &entries)
{
    if (entries.size() == 0) return;
    auto current = entries.back();
    if (current.isComplete) return;
    auto time = std::time(nullptr);
    auto local = std::localtime(&time);
    current.timeComplete = *local;
    current.isComplete = true;
    entries.back() = current;
}

void StartNew(std::vector<entry> &entries)
{
    CompleteLast(entries);
    std::string title;
    std::cin>>title;
    auto time = std::time(nullptr);
    auto local = std::localtime(&time);
    entries.push_back({title, *local});
}

void PrintEntries(std::vector<entry> &entries)
{
    for (auto pe = entries.begin(); pe != entries.end(); pe++)
    {
        std::cout<<pe->title<<":"<<std::endl;
        std::cout<<"start: "<<std::asctime(&(pe->timeStart));
        if (pe->isComplete)
            std::cout<<"complete: "<<std::asctime(&(pe->timeComplete));
        std::cout<<std::endl;
    }
}

int main()
{
    std::cout<<"Time manager."<<std::endl<<std::endl;
    std::cout<<"begin <title> - start new task"<<std::endl;
    std::cout<<"end - complete current task"<<std::endl;
    std::cout<<"status - info about all tasks"<<std::endl;
    std::cout<<"exit - exit"<<std::endl<<std::endl;

    std::vector<entry> entries;

    while (true)
    {
        std::string command;
        std::cin>>command;
        if (command == "exit") break;
        else if (command == "begin") StartNew(entries);
        else if (command == "end") CompleteLast(entries);
        else if (command == "status") PrintEntries(entries);
    }
}