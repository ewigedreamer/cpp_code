#include <stdio.h>
#include <iostream>
#include <map>
#include <string>

void printMap(std::map<int, std::string> &map)
{
    for(std::map<int, std::string>::iterator it = map.begin(); it != map.end(); ++it)
    {
        std::cout<<it->first<<" "<<it->second<<std::endl;
    }
}

int main()
{
    std::map<int, std::string> oMap;
    oMap.insert(std::make_pair<int, std::string>(10, "ten"));
    oMap.insert(std::make_pair<int, std::string>(11, "eleven"));
    oMap.insert(std::make_pair<int, std::string>(8, "eight"));
    oMap.insert(std::make_pair<int, std::string>(6, "six"));
    oMap.insert(std::make_pair<int, std::string>(7, "seven"));
    oMap.insert(std::make_pair<int, std::string>(12, "twelve"));
    oMap.insert(std::make_pair<int, std::string>(2, "two"));
    printMap(oMap);

    std::cout<<std::endl;

    std::map<int, std::string>::iterator itf = oMap.find(10);
    oMap.erase(itf);
    printMap(oMap);

    std::cout<<std::endl;

    oMap.erase(7);
    printMap(oMap);


    return 0;
}