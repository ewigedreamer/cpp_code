#include <stdio.h>
#include <iostream>
#include <map>
#include <string>

int main()
{
    std::map<int, std::string> apartPerson;
    apartPerson[2] = "ivanov";
    apartPerson[5] = "petrov";

    std::cout<<apartPerson[2]<<std::endl;

    apartPerson[2] = "sidorov";

    std::cout<<apartPerson[2]<<std::endl;

    return 0;
}