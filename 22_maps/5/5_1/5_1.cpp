#include <iostream>
#include <string>
#include <vector>
#include <map>

void split(
    std::vector<std::string> &vec,
    const  std::string  & source,
    const  std::string  & delimiter)
{
    if (source.size() == 0) return;
    size_t start = 0, end = 0;
    while (end != std::string::npos)
    {
        end = source.find(delimiter, start);
        vec.push_back(source.substr(start, (end == std::string::npos) ? std::string::npos : end - start));
        start = ((end > (std::string::npos - delimiter.size())) ? std::string::npos : end + delimiter.size());
    }
}

bool is_digit(const char c) { return c >= '0' && c <= '9'; }

bool has_data(std::vector<std::string> &v)
{
    if (v.size() == 0) return false;
    if (v.size() == 1 && v[0].length() == 0) return false;
    return true;
}

void print_vector(const std::vector<std::string> &v)
{
    for (int i = 0; i < v.size(); ++i)
    {
        std::cout<<v[i]<<std::endl;
    }
}

bool is_number(const std::string &str)
{
    return str.length() > 0 && is_digit(str[0]);
}

std::vector<std::string> make_vector()
{
    std::vector<std::string> v;
    return v;
}

void print_entry(const std::string &key, const std::map<std::string, std::vector<std::string> > &map)
{
    auto it = map.find(key);
    if (it != map.end())
    {
        std::cout<<"results:"<<std::endl;
        print_vector(it->second);
    }
    else
    {
        std::cout<<"no data in map!"<<std::endl;
    }
}

void add_entry(std::map<std::string, std::vector<std::string> > &map, const std::string &key, const std::string &value)
{
    auto it = map.find(key);
    if (it == map.end())
    {
        std::vector<std::string> v;
        v.push_back(value);
        map.insert(std::pair<std::string, std::vector<std::string> >(key, v));
    }
    else
    {
        it->second.push_back(value);
    }
}

int main()
{
    std::map<std::string, std::vector<std::string> > name_phone_map;
    std::map<std::string, std::vector<std::string> > phone_name_map;
    std::vector<std::string> buff;

    std::cout<<"type \"<number> <name>\" to save entry"<<std::endl;
    std::cout<<"type \"<number>\" or \"<name>\" to get saved data"<<std::endl;
    std::cout<<"type \"q\" to exit"<<std::endl;

    while (true)
    {
        std::string line;
        std::getline(std::cin, line);

        if (line == "q") break;

        buff.clear();
        split(buff, line, " ");

        if (!has_data(buff))
        {
            std::cout<<"please, type \"<number> <name>\""<<std::endl;
            continue;
        }

        if (buff.size() == 1)
        {
            auto word = buff[0];
            print_entry(word, is_number(word) ? phone_name_map : name_phone_map);
        }
        else
        {
            auto phone = buff[0];
            auto name = buff[1];
            if (!is_number(phone) || name.length() == 0)
            {
                std::cout<<"please, type \"<number> <name>\""<<std::endl;
                continue;
            }
            add_entry(phone_name_map, phone, name);
            add_entry(name_phone_map, name, phone);
            std::cout<<"entry has been created!"<<std::endl;
        }
    }

    return 0;
}