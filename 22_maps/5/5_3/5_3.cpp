#include <iostream>
#include <string>
#include <map>

void addChar(std::map<char, int> &map, const char c)
{
    auto it = map.find(c);
    if (it != map.end()) it->second++;
    else map.insert(std::pair<char, int>(c, 1));
}

void bakeWord(std::map<char, int> &map, const std::string &str)
{
    for (int i = 0; i < str.length(); ++i)
        addChar(map, str[i]);
}

bool compare(const std::map<char, int> &map1, const std::map<char, int> &map2)
{
    if (map1.size() != map2.size()) return false;
    auto it1 = map1.begin();
    auto it2 = map2.begin();
    while (true)
    {
        if (it1 == map1.end() || it2 == map2.end()) break;
        if (it1->first != it2->first) return false;
        if (it1->second != it2->second) return false;
        it1++;
        it2++;
    }
    return true;
}

int main()
{
    std::map<char, int> map1;
    std::map<char, int> map2;
    
    std::string word;

    std::cout<<"enter first word: ";
    std::cin>>word;
    bakeWord(map1, word);

    std::cout<<"enter second word: ";
    std::cin>>word;
    bakeWord(map2, word);

    if (compare(map1, map2))
        std::cout<<"true"<<std::endl;
    else
        std::cout<<"false"<<std::endl;

    return 0;
}