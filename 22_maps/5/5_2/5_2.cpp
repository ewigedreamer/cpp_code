#include <iostream>
#include <string>
#include <map>

int main()
{
    std::string input;
    std::map<std::string, int> reg;

    std::cout<<"type \"next\" for get next"<<std::endl;
    std::cout<<"type \"q\" for quit"<<std::endl;

    while (true)
    {
        std::cin>>input;

        if (input == "q") break;

        if (input == "next")
        {
            auto first = reg.begin();
            if (first == reg.end())
            {
                std::cout<<"queue is empty!"<<std::endl;
            }
            else
            {
                std::cout<<first->first<<std::endl;
                first->second--;
                if (first->second <= 0)
                {
                    reg.erase(first);
                }
            }
        }
        else
        {
            auto itf = reg.find(input);
            if (itf == reg.end())
            {
                reg.insert(std::pair<std::string, int>(input, 1));
            }
            else
            {
                itf->second++;
            }
        }
    }

    return 0;
}