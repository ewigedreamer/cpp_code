#include <stdio.h>
#include <iostream>
#include <map>
#include <string>

int main()
{
    std::pair<std::string, int> oPair("hello", 5);
    std::map<std::string, int> ourMap;
    ourMap.insert(oPair);
    std::cout<<ourMap["hello"]<<std::endl;

    ourMap.insert(std::pair<std::string, int>("world", 6));
    ourMap.insert(std::pair<std::string, int>("banana", 7));
    std::cout<<ourMap["world"]<<std::endl;

    ourMap.insert(std::pair<std::string, int>("hello", 8));
    std::cout<<ourMap["hello"]<<std::endl;

    ourMap["hello"] = 8;
    std::cout<<ourMap["hello"]<<std::endl;

    std::map<std::string, int>::iterator it = ourMap.find("hello");
    std::cout<<it->first<<" "<<it->second<<std::endl;

    return 0;
}