#include <iostream>
#include <string>

#define STR(s) #s

#define W_1 STR(Monday)
#define W_2 STR(Tuesday)
#define W_3 STR(Wednesday)
#define W_4 STR(Thursday)
#define W_5 STR(Friday)
#define W_6 STR(Saturday)
#define W_7 STR(Sunday)

#define WEEK(n) W ## _ ## n
#define PRINT_WEEK(n) std::cout<<WEEK(n)<<std::endl
#define CASE(n) case n: PRINT_WEEK(n); break;

#define ERR STR(wrong number)
#define PRINT_ERR std::cout<<ERR<<std::endl
#define DEFAULT default: PRINT_ERR; break;

int main()
{
    int n;
    std::cin >> n;
    switch (n)
    {
        CASE(1)
        CASE(2)
        CASE(3)
        CASE(4)
        CASE(5)
        CASE(6)
        CASE(7)
        DEFAULT
    }

    return 0;
}