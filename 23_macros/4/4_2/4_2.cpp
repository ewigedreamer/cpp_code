// #define SPRING
// #define SUMMER
#define AUTUMN
// #define WINTER

#define STR(a) #a
#define PRINT(a) int main() { std::cout << STR(a) << std::endl; return 0; }

#include <iostream>

#if defined SPRING
PRINT(spring)
#elif defined SUMMER
PRINT(summer)
#elif defined AUTUMN
PRINT(autumn)
#elif defined WINTER
PRINT(winter)
#endif